#use "pq.ml"

type graphe = int list array
type sommet = int

let ordre (g : graphe) : int = Array.length g

let voisins (g : graphe) (s : sommet) : int list = g.(s)

let g0 = [|[1; 3; 2]; [0; 2]; [3; 2; 1]; [2; 0]|]

let g1 = [|[1; 2]; [0; 2]; [0; 1; 3; 4; 5]; [2; 4; 5]; [2; 3; 5]; [2; 3; 4]|]

let mex (liste : int list) (n : int) : int =
  let existe = Array.make n false in
  List.iter (fun k -> existe.(k) <- true) liste;
  let col = ref 0 in
  while existe.(!col) do incr col done;
  !col

let grundy (g : graphe) (p : int array) : int array =
  let n = ordre g in
  let cols = Array.make n n in
  for i = 0 to (n-1) do
    let s = p.(i) in
    let col_voisins = List.map (fun k -> cols.(k)) (voisins g s) in
    cols.(s) <- mex col_voisins (n+1)
  done;
  cols
  
let suiteSimpliciale g =
  let n = ordre g in
  Array.init n (fun k ->  k)
  
let voisinsGauche g sigma =
  let n = ordre g in
  let tailleK = Array.make n 1 in
  let voisinsG = Array.make n [] in
  let sigInv = Array.make n 0 in
  for i = 0 to (n-1) do sigInv.(sigma.(i)) <- i done;
  let traiter s t = 
    if  sigInv.(t) > sigInv.(s) then begin
      tailleK.(t) <- tailleK.(t) + 1;
      voisinsG.(t) <- s :: voisinsG.(t) 
   end in
  for i = 0 to (n-1) do
    let s = sigma.(i) in
    List.iter (traiter s) (voisins g s)
  done;
  voisinsG, tailleK

let sigma0 = [|0; 1; 2; 3|]

let cliquesMax g = 
  let n = ordre g in
  let sigma = suiteSimpliciale g in
  let vG, tK = voisinsGauche g sigma in
  let clqMax = ref [] in
  let prevu = Array.make n 0 in
  for i = (n-1) downto 0 do
    let s = sigma.(i) in
    if tK.(s) > prevu.(s) 
    then clqMax := (s::vG.(s)):: !clqMax;
    if tK.(s) > 1 
    then let t = List.hd vG.(s) in 
         prevu.(t) <- max (tK.(s) - 1) prevu.(t)
  done;
  !clqMax

 let stableMax g = 
  let n = ordre g in
  let sigma = suiteSimpliciale g in
  let vG, tK = voisinsGauche g sigma in
  let stbl = ref [] in
  let recouvK = ref [] in
  let pris = Array.make n false in
  for i = (n-1) downto 0 do
    let s = sigma.(i) in
    if not pris.(s) then begin
      stbl := s :: !stbl;
      let clique = s :: vG.(s) in
      recouvK := clique :: !recouvK;
      List.iter (fun k -> pris.(k) <- true) clique
    end
  done;
  !stbl, !recouvK

let _ = cliquesMax g1

let _ = stableMax g1
































