type noeud = Fin | N of {      valeur : int  ; 
                         mutable suiv : noeud; 
                         mutable prec : noeud}
                        
type ldd = {mutable prem : noeud;
            mutable dern : noeud}
            
exception Noeud_vide

let vlr = function
  |Fin -> raise Noeud_vide
  |N n -> n.valeur

let avt = function
  |Fin -> raise Noeud_vide
  |N n -> n.prec

let apr = function
  |Fin -> raise Noeud_vide
  |N n -> n.suiv

let change_prec  nn = function
  |Fin -> raise Noeud_vide
  |N n -> n.prec <- nn

let change_suiv  nn = function
  |Fin -> raise Noeud_vide
  |N n -> n.suiv <- nn

let cree_ldd () = {prem = Fin; dern = Fin}

let un_seul x = 
  let nn = N {prec = Fin; suiv = Fin; valeur = x} in 
  {prem = nn; dern = nn}

let voir listedd =
  let rec aux = function
    |Fin -> print_newline ()
    |N n -> print_int n.valeur;
	        print_string ", ";
		    aux n.suiv in
  aux listedd.prem

let ajoute_fin x listedd =
  let nnf = listedd.dern in
  let newnn = N {prec = nnf; suiv = Fin; valeur = x} in
  listedd.dern <- newnn;
  match nnf with
  |Fin -> listedd.prem <- newnn
  |N nf -> nf.suiv <- newnn
  
let ajoute_debut x listedd =
  let nnd = listedd.prem in
  let newnn = N {prec = Fin; suiv = nnd; valeur = x} in
  listedd.prem <- newnn;
  match nnd with
  |Fin -> listedd.dern <- newnn
  |N nd -> nd.prec <- newnn
  
let ajoute_avant x nn listedd =
  let nnp = avt nn in
  let newnn = N {prec = nnp; suiv = nn; valeur = x} in
  change_prec newnn nn;
  match nnp with
  |Fin -> listedd.prem <- newnn
  |N np -> np.suiv <- newnn
  
let ajoute_apres x nn listedd =
  let nns = apr nn in
  let newnn = N {prec = nn; suiv = nns; valeur = x} in
  change_suiv  newnn nn;
  match nns with
  |Fin -> listedd.dern <- newnn
  |N ns -> ns.prec <- newnn
  
let enleve nn listedd =
  match avt nn, apr nn with
  |Fin, Fin -> listedd.prem <- Fin;
               listedd.dern <- Fin
  |Fin, nns -> change_prec Fin nns;
               listedd.prem <- nns
  |nnp, Fin -> change_suiv Fin nnp;
               listedd.dern <- nnp
  |nnp, nns -> change_suiv nns nnp;
               change_prec nnp nns
              
  
  
let l = cree_ldd ()
let _ =  ajoute_fin 3 l;
         ajoute_fin 5 l;
         ajoute_debut 2 l;
         let nn = apr l.prem in
         ajoute_apres 7 nn l;
         ajoute_avant 11 nn l;
         enleve nn l;
         voir l

     
          
