type element_tas = {ind : int; mutable prio : int}

type priorite = {mutable nb : int;
                        tas : element_tas array;
                        pos : int array}
                        
let creerPQ n = 
  let t = Array.init n (fun k -> {ind = k; prio = 0}) in
  let p = Array.init n (fun k -> k) in
  {nb = n; tas = t; pos = p}

let echange i j pq =
  (* i et j sont des indices pour le tas *)
  let ki = pq.tas.(i).ind in
  let kj = pq.tas.(j).ind in
  pq.pos.(ki) <- j;
  pq.pos.(kj) <- i;
  let e = pq.tas.(j) in
  pq.tas.(j) <- pq.tas.(i);
  pq.tas.(i) <- e
 
  
let rec monter i pq = 
  assert (i < pq.nb);
  if i > 0 then
    let j = (i - 1) / 2 in
    if pq.tas.(i).prio > pq.tas.(j).prio then begin
      echange i j pq;
      monter j pq
    end
    
let grand_fils i pq =
  assert (2*i < pq.nb);
  if 2*i = pq.nb - 1 then 
    2*i
  else
    let e1 = pq.tas.(2*i) in
    let e2 = pq.tas.(2*i + 1) in
    if e1.prio > e2.prio then 2*i else (2*i + 1)

let rec descendre i pq =
  if (2*i < pq.nb) then
    let j = grand_fils i pq in
    if pq.tas.(i).prio < pq.tas.(j).prio then 
    echange i j pq;
    descendre j pq

let incrP k pq = 
  let i = pq.pos.(k) in
  let e = pq.tas.(i) in
  pq.tas.(i).prio <- e.prio + 1;
  monter i pq
  
let maxPQ pq = 
  let n = pq.nb in
  let k = pq.tas.(0).ind in
  echange 0 (n-1) pq;
  pq.nb <- n-1;
  descendre 0 pq;
  k
