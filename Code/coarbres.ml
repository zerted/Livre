type ope = U | P

type coarbre = Fe of int
             | Nd of ope * coarbre list 

type coarbreBin = F of int
                | N of coarbreBin * ope * coarbreBin 

let c1 = Nd (U,[Fe 0; Fe 1; Nd (P, [Fe 2; Fe 3])])

let c0 = Nd (P, [c1; Nd (U, [Fe 4; Fe 5]); Fe 6])

let rec ordreG c =
  let f acc cc = acc + ordreG cc in
  match c with
    |Fe _ -> 1
    |Nd (_, l) -> List.fold_left f 0 l
    
let rec coa2bin = function
  |Fe k -> F k
  |Nd ( op,  [c1; c2]) -> N (coa2bin c1, op, coa2bin c2)
  |Nd (op, c1::reste) -> N (coa2bin c1, op, coa2bin (Nd (op, reste)))
  |_ -> failwith "Le coarbre n'est pas canonique" 
  
let rec ordre c =
  match c with
  |F _ -> 1
  |N (c1, _, c2) -> ordre c1 + ordre c2
  
let taille c =
  let rec sETa = function
    |F _ -> 1, 0
    |N (c1, U, c2) -> let s1, a1 = sETa c1 in
                      let s2, a2 = sETa c2 in
                      s1 + s2, a1 + a2
    |N (c1, P, c2) -> let s1, a1 = sETa c1 in
                      let s2, a2 = sETa c2 in
                      s1 + s2, a1 + a2 + s1*s2 in
  let s, a = sETa c in s + a

let rec compl = function
  |F k -> F k
  |N (g, U, d) -> N (compl g, P, compl d)
  |N (g, P, d) -> N (compl g, U, compl d)
  
let rec jumeaux = function
  |F k -> failwith "Le graphe est élémentaire"
  |N (F k, op, F l) -> k, l
  |N (F k, op, d) -> jumeaux d
  |N (g, op, d) -> jumeaux g

let rec omega = function
  |F k -> 1
  |N (g, U, d) -> max (omega g) (omega d)
  |N (g, P, d) -> (omega g) + (omega d)

let cliqueMax coa = 
  let rec wETclique =function
    |F k -> 1, [k]
    |N (g, U, d) -> let wg, clg = wETclique g in
                    let wd, cld = wETclique d in
                    if wg > wd
                    then wg, clg
                    else wd, cld
    |N (g, P, d) -> let wg, clg = wETclique g in
                    let wd, cld = wETclique d in
                    wg + wd, clg @ cld in
  let _, cl = wETclique coa in cl 

let graphe c = 
  let n = ordre c in
  let noms = Array.make n 0 in
  let g = Array.make n [] in
  let rec remplir init = function
    |F k -> noms.(init) <- k; init + 1
    |N (cg, U, cd) -> let init1 = remplir init cg in
                      remplir init1 cd
    |N (cg, P, cd) -> let init1 = remplir init cg in
                     let init2 = remplir init1 cd in
                     for i = init to init1 - 1 do
                       for j = init1 to init2 - 1 do
                         g.(i) <- j :: g.(i);
                         g.(j) <- i :: g.(j) 
                       done
                     done;
                     init2 in
    let _ = remplir 0 c in
    g, noms
  
let cb0 = coa2bin c0 

let coloration coa = 
  let n = ordre coa in
  let col = Array.make n 0 in
  let rec colorer init = function
    |F k -> col.(k) <- init;
            init + 1
    |N (g, U, d) -> let ig = colorer init g in
                    let id = colorer init d in
                    max ig id
    |N (g, P, d) -> let ig = colorer init g in
                    let id = colorer ig d in
                    id in
  let _ = colorer 0 coa in
  col 


