%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
Dans ce chapitre on pose les définitions et les notations. Les notions exposées sont classiques.

Si $S$ est un ensemble, $\pp(S)$\index{$\pp(S)$} est l'ensemble des parties à 2 éléments de $S$.

Les graphes considérés ici sont non orientés, sans arêtes multiples ni boucles.
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
\section{Graphes}
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
\begin{definition}{Graphe}{}
Un graphe est un couple $G=(S, A)$ où $S$ est un ensemble fini non vide, les sommets, et $A$ est une partie de $\pp(S)$, les arêtes.
\end{definition}
%-------------------------------------------------------------------------------
On notera $s\sim_G t$ \index{$s \sim_G t$} pour signifier que $\{s, t\}$ est une arête de $G$ ; on dira aussi que $s$ et $t$ sont adjacents (dans $G$).

On notera $s \not\sim_G t$  \index{$s \not\sim_G t$}si $\{s, t\}$ n'est pas une arête de $G$.

S'il n'y pas d'ambiguïté, on notera plus simplement $s \sim t$ et $s \not\sim t$.
%-------------------------------------------------------------------------------
\begin{definition}{Ordre et taille}{}
L'ordre de $G=(S, A)$ est le nombre de sommets, $|S|$.

La taille de $G=(S, A)$ est la somme des cardinaux de $S$ et de $A$, on la notera $|G|=|S|+|A|$.
\end{definition}
%-------------------------------------------------------------------------------
\begin{definition}{Voisinages}{}
\begin{itemize}
\item Dans un graphe $G=(S, A)$, le voisinage  d'un sommet $s$, noté $N_G(s)$,  est l'ensemble des sommets liés à $s$ par une arête :

$N_G(s) =\bigl\{t\in S\ ;\ s \sim t\bigr\}$.\index{$N_G(s)$}

\item Le voisinage fermé d'un sommet $s$, noté $\overline{N_G}(s)$,  est l'union de $N_G(s)$ et de $\{s\}$. \index{$\overline{N_G}(s)$}
\end{itemize}
\end{definition}
%-------------------------------------------------------------------------------
Le voisinage de $s$ est aussi appelé voisinage ouvert de $s$.

Ici encore, on omettra le plus souvent l'indice $G$ si le contexte ne permet pas d'ambiguïté.
%-------------------------------------------------------------------------------
\begin{definition}{Degré}{}
\begin{itemize}
\item Dans un graphe $G=(S, A)$, le degré  d'un sommet est son nombre de voisins : $\text{deg}_G(s) = |N_G(s)|$.

\item Le degré maximum d'un graphe $G$, noté $\Delta(G)$, est le maximum des degrés de ses sommets :
$\Delta(G) = \max\bigl\{\text{deg}_G(s)\ ;\ s\in S\bigr\}$.\index{$\Delta(G)$}
\end{itemize}
\end{definition}
%-------------------------------------------------------------------------------
\begin{definition}{Sommets jumeaux}{}
Dans un graphe $G=(S, A)$, deux sommets distincts $s$ et $s'$ sont jumeaux faibles s'ils ont le même voisinage, $N_G(s)=N_G(s')$.

Les sommets sont des jumeaux forts s'ils ont le même voisinage fermé.
\end{definition}
%-------------------------------------------------------------------------------
Deux sommets sont jumeaux s'ils sont jumeaux faibles ou jumeaux forts.
%-------------------------------------------------------------------------------
\begin{exo}
\begin{enumerate}
   \item Prouver que si $s$ et $s'$ sont jumeaux faibles dans $G$ alors $s\not\sim_G s'$.
   \item Prouver que si $s$ et $s'$ sont jumeaux forts dans $G$ alors $s\sim_G s'$.
\end{enumerate}
%-------------------------------------------------------------------------------
\reponse
%-------------------------------------------------------------------------------
\begin{enumerate}
   \item $s$ n'est pas un voisin de lui même, $s \notin N(s)$.

   Si $N(s)=N(s')$ alors $s\notin N(s')$ donc $s\not\sim_G s'$.
   \item On a $s\in\overline N(s)$ donc, pour $\overline N(s) =\overline N(s')$, $s\in \overline N(s')$.

   Pour $s\ne s'$ cela implique $s\in N(s)$ donc $s\sim s'$.
\end{enumerate}
\end{exo}
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
\subsection{Implémentation}
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
\begin{itemize}
   \item Les sommets d'un graphe d'ordre $n$ sont les entiers de 0 à $n-1$.
   \item Les graphes sont représentés par le tableau des listes d'adjacence.
%-------------------------------------------------------------------------------
\begin{ocaml}
type graphe = int list array
\end{ocaml}
%-------------------------------------------------------------------------------
   \item On utilisera les fonctions de calcul de l'ordre d'un graphe
%-------------------------------------------------------------------------------
\begin{ocaml}
let ordre (g : graphe) : int = Array.length g
\end{ocaml}
%-------------------------------------------------------------------------------
et la liste des voisins
%-------------------------------------------------------------------------------
\begin{ocaml}
let voisins (g : graphe) (s : sommet) : int list = g.(s)
\end{ocaml}
%-------------------------------------------------------------------------------
\end{itemize}
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
\section{Chemins}
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
\begin{definition}{Chemin, chemin élémentaire}{}
$s$ et $t$ sont deux sommets d'un graphe $G$.
\begin{itemize}
   \item Un chemin entre d$s$ et $t$ est une suite finie de sommets telle que
   \begin{itemize}
      \item le premier sommet est $s$,
      \item le dernier sommet est $t$,
      \item chaque sommet, sauf le premier, est adjacent au précédent
   \end{itemize}
   \item Le chemin est élémentaire si les sommets sont distincts.
\end{itemize}
\end{definition}
%-------------------------------------------------------------------------------
$(s_0, s_1, \ldots, s_{p-1}, s_p)$ est un chemin entre $s$ et $t$ si

$s_0=s$, $s_p=t$ et $s_{i-1}\sim s_i$ pour tout $i\in \{1, 2, \ldots, p\}$.
%-------------------------------------------------------------------------------
\begin{exo}[Chemins élémentaires]
\begin{enumerate}
   \item Prouver que, dans un chemin élémentaire, les arêtes formées de deux sommets consécutifs sont distinctes.
   \item Prouver que s'il existe un chemin entre $s$ et $t$ alors il existe un chemin élémentaire.
\end{enumerate}
%-------------------------------------------------------------------------------
\reponse
%-------------------------------------------------------------------------------
\begin{enumerate}
   \item Si une arête apparaît deux fois dans le chemin, ses extrémités définissent 4 sommets du chemin ou 3 si les deux apparitions sont adjacentes. Or une arête ne contient que 2 sommets donc il existe deux sommets égaux dans le chemin.
   \item Si un chemin n'est pas élémentaire alors il passe 2 fois par le même sommet $u$. En enlevant les sommet entre ces deux occurrences de $u$, on aboutit à un chemin plus court entre $s$ et $t$. On conclut par récurrence sur la longueur du chemin.
\end{enumerate}
\end{exo}
%-------------------------------------------------------------------------------
\begin{definition}{Longueur}{}
La longueur d'un chemin est la longueur de la suite diminuée de 1, c'est le nombre d'arêtes entre deux sommets consécutifs..
\end{definition}
%-------------------------------------------------------------------------------
\begin{definition}{Graphe connexe}{}

Un graphe est connexe s'il existe un chemin entre toute paire de sommets.
\end{definition}
%-------------------------------------------------------------------------------
\begin{definition}{Distance et diamètre}{}
\begin{itemize}
   \item Dans un graphe connexe, la distance entre deux sommets est la longueur minimale d'un chemin entre deux sommets.
   \item Le diamètre d'un graphe connexe est la distance maximale entre deux sommets.
\end{itemize}
\end{definition}
%-------------------------------------------------------------------------------
\begin{definition}{Chemin hamiltonien}{}

Un chemin hamiltonien d'un graphe est un chemin élémentaire qui passe par tous les sommets.
\end{definition}
%-------------------------------------------------------------------------------
Comme il doit être élémentaire, un chemin hamiltonien passe une fois et une seule par chaque sommet.
%-------------------------------------------------------------------------------
\begin{definition}{Cycle et cycle élémentaire}{}
\begin{itemize}
   \item Un chemin entre un sommet et lui-même est un cycle.
   \item Le cycle est élémentaire si les sommets qu'il parcourt sont distincts (à l'exception des extrémités) et s'il est de longueur différente de deux.
\end{itemize}
\end{definition}
%-------------------------------------------------------------------------------
La condition sur la longueur interdit les cycles  $s\sim s' \sim s$.

Comme il n'y a pas de boucle, un cycle de longueur 1 ne peut pas exister.
%-------------------------------------------------------------------------------
\begin{definition}{Cycle hamiltonien}{}

Un cycle hamiltonien d'un graphe est un cycle élémentaire qui passe par tous les sommets.
\end{definition}
%-------------------------------------------------------------------------------
\begin{definition}{Graphe hamiltonien}{}

Un graphe est hamiltonien s'il admet un cycle hamiltonien.

On convient qu'un graphe élémentaire est hamiltonien.
\end{definition}
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
\section{Graphes classiques}
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
\begin{minipage}{0.5\linewidth}
Un graphe de type $E_n$ ($E$ pour \textsc{empty}) est un graphe à $n$ sommets et sans arêtes.\index{$E_n$}
\end{minipage}
\begin{minipage}{0.5\linewidth}
\begin{center}
\begin{tikzpicture}[scale=0.8]
\node (x) at (180 : 2.5) {$E_5$};
\foreach \i in {0, 1, 2, 3, 4} \node[s] (\i) at (\i*72 : 1.5) {$\i$};
\end{tikzpicture}
\end{center}
\end{minipage}

\medskip

\begin{minipage}{0.5\linewidth}
Un graphe de type $K_n$ ($K$ pout \textsc{komplete}, en allemand) est un graphe complet à $n$ sommets, l'ensemble des arêtes est $\pp(S)$\index{$K_n$} où $S$ est l'ensemble des sommets.
\end{minipage}
\begin{minipage}{0.5\linewidth}
\begin{center}
\begin{tikzpicture}[scale=0.8]
\node (x) at (180 : 2.5) {$K_5$};
\foreach \i in {0, 1, 2, 3, 4} \node[s] (\i) at (\i*72 : 1.5) {$\i$};
\draw (0) --(1) -- (2) -- (3) -- (4) -- (0);
\draw (0) --(2) -- (4) -- (1) -- (3) -- (0);
\end{tikzpicture}
\end{center}
\end{minipage}

\medskip

\begin{minipage}{0.5\linewidth}
Un graphe de type $C_n$ ($C$ pour \textsc{cycle}) est un graphe correspondant à un cycle élémentaire à $n$ sommets, \index{$C_n$} (il n'existe pas de graphe de type $C_1$ ou $C_2$).
\end{minipage}
\begin{minipage}{0.5\linewidth}
\begin{center}
\begin{tikzpicture}[scale=0.8]
\node (x) at (180 : 2.5) {$C_5$};
\foreach \i in {0, 1, 2, 3, 4} \node[s] (\i) at (\i*72 : 1.5) {$\i$};
\draw (0) --(1) -- (2) -- (3) -- (4) -- (0);
\end{tikzpicture}
\end{center}
\end{minipage}

\medskip

\begin{minipage}{0.5\linewidth}
Un graphe de type $P_n$ ($P$ pour \textsc{path}) est un graphe correspondant à un chemin élémentaire à $n$ sommets\index{$P_n$}, il est de longueur $n-1$.
\end{minipage}
\begin{minipage}{0.5\linewidth}
\begin{center}
\begin{tikzpicture}[scale=0.8]
\node (x) at (180 : 2.5) {$P_5$};
\foreach \i in {0, 1, 2, 3, 4} \node[s] (\i) at (\i*72 : 1.5) {$\i$};
\draw (0) --(1) -- (2) -- (3) -- (4);
\end{tikzpicture}
\end{center}
\end{minipage}

\medskip

\begin{minipage}{0.5\linewidth}
Un graphe de type $S_n$ ($S$ pour \textsc{star}) est un graphe  à $n$ sommets\index{$S_n$} et à $n-1$ arêtes, celles-ci  joignent un sommet $s_0$ à tous les autres.
\end{minipage}
\begin{minipage}{0.5\linewidth}
\begin{center}
\begin{tikzpicture}[scale=0.8]
\node[s] (0) at (0 : 1.5) {$0$};
\node (x) at (180 : 2.5) {$S_5$};
\foreach \i in { 1, 2, 3, 4}
  {\node[s] (\i) at (\i*72 : 1.5) {$\i$}; \draw (0) -- (\i);};
\end{tikzpicture}
\end{center}
\end{minipage}
